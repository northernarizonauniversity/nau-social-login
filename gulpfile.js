const gulp = require('gulp');
const gutil = require('gulp-util');
const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const less = require('gulp-less');
const source = require('vinyl-source-stream');
const sourcemaps = require('gulp-sourcemaps');
const tsify = require('tsify');
const watchify = require('watchify');
const watchLess = require('gulp-watch-less');

let paths = {
  pages: ['src/*.html'],
  images: ['images/**/*'],
  fa: ['less/font-awesome/fonts/*']
};

gulp.task('html', function () {
  return gulp.src(paths.pages)
    .pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
  return gulp.src(paths.images)
    .pipe(gulp.dest('dist/images'));
});

gulp.task('font-awesome', function () {
  // return gulp.src(paths.fa)
  //   .pipe(gulp.dest('dist/fonts'));
});

gulp.task('less', function () {
  return gulp.src('less/main.less')
    // .pipe(watchLess('less/main.less'))
    .pipe(less())
    .pipe(gulp.dest('dist'));
});

let watchedBrowserify = watchify(browserify({
  basedir: '.',
  debug: true,
  entries: ['src/main.ts'],
  cache: {},
  packageCache: {},
  standalone: 'nau'
}).plugin(tsify));

let bundle = function () {
  return watchedBrowserify
    .transform('babelify', {
      presets: ['es2015'],
      extensions: ['.ts']
    })
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist'));
};

gulp.task('bundle', bundle);
gulp.task('default', ['html', 'images', 'font-awesome', 'less'], bundle);
watchedBrowserify.on('update', bundle);
watchedBrowserify.on('log', gutil.log);
