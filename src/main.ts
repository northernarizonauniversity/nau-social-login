import * as firebase from "firebase";
import Modal from "./Modal";
// import * as jQuery from "jquery";
declare var jQuery: any;

// Provides helpers for dealing with native DOM manipulation
class Helper {
  static wrap(content: string, tag?: string): HTMLElement {
    tag = tag || 'div'

    let contentHolder: HTMLElement = document.createElement(tag);
    contentHolder.innerHTML = content;
    return contentHolder;
  }

  static insert(content: HTMLElement | string, el?: HTMLElement | string): HTMLElement | null {
    el = el || document.body;

    if (typeof content === "string") content = Helper.wrap(content)
    if (typeof el === "string") el = document.getElementById(el);

    if (el) {
      el.appendChild(content);
      return el;
    } else {
      return null;
    }
  }
}

// Interfaces for use with NAUSocialLogin
interface GenericHandler {
  (result: any): any;
}

interface Providers {
  [propName: string]: any
}

interface NAUSocialLoginConfig {
  apiKey: string;
  authDomain: string;
  databaseURL: string;
  storageBucket: string;
  facebookAuthScope: Array<string>;
  googleAuthScope: Array<string>;
  onAuthStateChanged?: GenericHandler;
  onInitializeApp?: GenericHandler;
  onSignInFailure?: GenericHandler;
  onSignInSuccess?: GenericHandler;
}

// NAUSocialLogin
export class NAUSocialLogin {
  static Modal = Modal;

  config: NAUSocialLoginConfig;
  firebase: any;
  providers: Providers;
  providerMenuModal: Modal;
  app: any;
  auth: any;
  user: any;

  constructor(options: any) {
    this.config = Object.assign({
      apiKey: options.key || null,
      authDomain: options.app ? `${options.app}.firebaseapp.com` : null,
      databaseURL: options.app ? `https://${options.app}.firebaseio.com` : null,
      storageBucket: options.app ? `${options.app}.appspot.com` : null,
      facebookAuthScope: ["public_profile", "email"],
      googleAuthScope: ["profile", "email"]
    }, options);

    console.log(this.config, options);

    this.firebase = firebase;
    this.app = firebase.initializeApp(this.config);
    this.auth = this.app.auth();

    this.auth.onAuthStateChanged(this.onAuthStateChanged);
    this.onSignInSuccess = this.config.onSignInSuccess || this.onSignInSuccess;
    this.onSignInFailure = this.config.onSignInFailure || this.onSignInFailure;

    // Init providers
    let providers: Providers = <Providers>{
      facebook: new firebase.auth.FacebookAuthProvider(),
      google: new firebase.auth.GoogleAuthProvider(),
      email: new firebase.auth.EmailAuthProvider()
    };

    if (this.config.facebookAuthScope && this.config.facebookAuthScope.length) {
      this.config.facebookAuthScope.forEach((scope: string) => providers["facebook"].addScope(scope));
    }

    if (this.config.googleAuthScope && this.config.googleAuthScope.length) {
      this.config.googleAuthScope.forEach((scope: string) => providers["google"].addScope(scope));
    }

    this.providers = providers;

    // Init Modal / events
    let menu: HTMLElement = this.getProviderMenu(Object.keys(this.providers));
    this.providerMenuModal = new Modal({
      content: menu,
      className: 'nsl-nau'
    });

    Object.keys(this.providers).forEach((provider) => {
      document.addEventListener(`nsl-btn-provider-${provider}.click`, (event) => {
        this.onProviderSelect(provider);
      });
    });

    // Done
    this.trigger("onInitializeApp", this.app);
  }

  /* Default Event Handlers */
  /**
   * Called any time the Firebase authentication status changes. Not super useful unless you are
   * doing something weird.
   */
  onAuthStateChanged(user: any): void {
    this.trigger("onAuthStateChanged", user);
    if (this.config.onAuthStateChanged) this.config.onAuthStateChanged(user)
  }

  /**
   * Called when a user successfully authenticates with Firebase. This is the one most
   * implementations will be intersted in.
   */
  onSignInSuccess(result: any): any {
    console.info("Sign in Success", result);
    this.user = result.user;

    this.trigger("onSignInSuccess", result);
    if (this.config.onSignInSuccess) this.config.onSignInSuccess(result)

    return result;
  }

  /**
   * Called when Firebase authentication fails
   */
  onSignInFailure(error: any): void {
    console.info("Sign in Failure", error);

    if (error.code === "auth/account-exists-with-different-credential") {
      let pendingCred: string = error.credential;
      let email: string = error.email;

      this.auth.fetchProvidersForEmail(email)
        .then((providers: Array<string>) => {
          let provider: string = this.providers[providers[0]];
          return this.auth.signInWithPopup(provider);
        })
        .then((result: any) => {
          this.user = result.user;
          return this.user.link(pendingCred);
        })
        .then(() => {
          this.trigger("onSignInLinkSuccess", this.user);
        });
    } else {
      // Some other error. Let the user know.
      this.trigger("onSignInFailure", error);
      if (this.config.onSignInFailure) this.config.onSignInFailure(error)
      return null;
    }
  }

  /**
   * Called when a user selects a provider from the provider menu.
   */
  onProviderSelect(provider: string): void {
    // TODO: option for redirect?
    this.trigger("onProviderSelect", provider);

    if (provider === "email") {
      let email: string = (<HTMLInputElement>document.getElementById("nsl-input-email")).value;
      let password: string = (<HTMLInputElement>document.getElementById("nsl-input-password")).value;

      return this.auth.signInWithEmailAndPassword(email, password)
        .then(this.onSignInSuccess.bind(this))
        .catch(this.onSignInFailure.bind(this));
    } else {
      this.signInWithPopup(provider);
    }
  }

  /**
   * Firebase native method. Opens the actual Firebase popup menu for provider authentication
   */
  signInWithPopup(provider: any): any {
    provider = this.providers[provider] || provider;

    return this.auth.signInWithPopup(provider)
      .then(this.onSignInSuccess.bind(this))
      .catch(this.onSignInFailure.bind(this));
  }

  /**
   * Opens the provider modal
   */
  openProviderMenuModal(): void {
    this.providerMenuModal.open();
  }

  /**
   * Closes the provider modal
   */
  closeProviderMenuModal(): void {
    this.providerMenuModal.close();
  }

  /**
   * Opens the provider menu and closes again when a provider is selected.
   */
  signInWithModal(): void {
    let name: string = 'onSignInSuccess.NAUSocialLogin';
    let handler: GenericHandler = (event: Event) => {
      document.removeEventListener(name, handler);
      this.closeProviderMenuModal();
    };

    document.addEventListener(name, handler);
    this.openProviderMenuModal();
  }

  /* Private / Protected */

  /**
   * Triggers the same event using both a native JS Event on document as well
   * as a jQuery custom event if available.
   */
  protected trigger(name: string, data?: any) {
    name += ".NAUSocialLogin";

    let event:any;

    try {
      event = data ?
        new CustomEvent(name, { detail: data }) :
        new Event(name)
    } catch (e) {
      event = document.createEvent('Event');
      event.initEvent(name, false, true);
    }

    // Standard JS events
    document.dispatchEvent(event);

    // Trigger a jQuery event if it's available.
    if (jQuery) jQuery(document).trigger(name, data || null);
  }

  /**
   * Creates the provider menu buttons for the given providers.
   */
  private getProviderMenu(providers: Array<string> = ["google", "facebook", "email"]): HTMLElement {
    let content: string = "";
    let email: boolean = false;


    providers.forEach((provider) => {
      console.log(provider);
      switch (provider) {
        case "email":
          email = true;
          return;
        default:
          let name: string = provider.charAt(0).toUpperCase() + provider.slice(1);
          content += `
            <button
              id="nsl-btn-provider-${provider}"
              class="${provider}"
              onclick="document.dispatchEvent(
                new Event('nsl-btn-provider-${provider}.click')
              )"
            >
              <i class="icon nsl-${provider}"></i>
              Sign in with ${name}
            </button>`;
      }
    })

    if (email) {
      content += `
        <div class="nsl-provider-email">
          <label for="nsl-input-email">Email</label>
          <input type="text" id="nsl-input-email" name="nsl-input-email" placeholder="email" />
          <label for="nsl-input-password">Password</label>
          <input type="password" id="nsl-input-password" name="nsl-input-password" placeholder="password" />

          <button id="nsl-btn-provider-email" class="email"
            onclick="document.dispatchEvent(
              new Event('nsl-btn-provider-email.click')
            )"
          >Sign in with Email</button>
        </div>`;
    }

    let menu: HTMLElement = Helper.wrap(content);
    menu.className = "social-wrap";

    return menu;
  }
}
