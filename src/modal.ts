interface ModalConfig {
  className: string,
  closeButton: boolean,
  content: HTMLElement | string,
  maxWidth: number,
  minWidth: number,
  overlay: boolean
}

export default class Modal  {
  config: ModalConfig;
  overlay: HTMLElement;
  modal: HTMLElement;
  closeButton: HTMLElement;
  transitionEnd = this.transitionSelect();

  constructor(options: any = {}) {
    this.config = Object.assign({
      className: "",
      closeButton: true,
      content: "",
      maxWidth: 600,
      minWidth: 260,
      overlay: true
    }, options);
  }

  open(): void {
    // Build the modal and initialize the event listeners
    this.buildOut();
    this.initializeEvents();

    window.getComputedStyle(this.modal).height;

    // Check if the modal is taller than the window. Add anchor class if it is.
    this.modal.className += " nsl-open";
    this.overlay.className += " nsl-open";

    if (this.modal.offsetHeight > window.innerHeight)
      this.modal.className += " nsl-anchored";
  }

  close(): void {
    // Remove the open class name
    this.modal.className = this.modal.className.replace(" nsl-open", "");
    this.overlay.className = this.overlay.className.replace(" nsl-open", "");

    // Remove nodes from DOM after CSS transition event
    this.modal.addEventListener(this.transitionEnd, () =>
      this.modal.parentNode.removeChild(this.modal)
    );
    this.overlay.addEventListener(this.transitionEnd, () => {
      if (this.overlay.parentNode) this.overlay.parentNode.removeChild(this.overlay)
    });
  }

  private transitionSelect(): string {
    let el = document.createElement("div");
    if (el.style.WebkitTransition) return "webkitTransitionEnd";
    if (el.style.OTransition) return "oTransitionEnd";
    return "transitionend";
  }

  private buildOut () {
    let contentHolder: HTMLElement;
    let docFrag: DocumentFragment = document.createDocumentFragment();

    // modal element
    this.modal = document.createElement("div");
    this.modal.className = "nsl-modal " + this.config.className;
    this.modal.style.minWidth = this.config.minWidth + "px";
    this.modal.style.maxWidth = this.config.maxWidth + "px";

    // Create content area and append to modal
    contentHolder = document.createElement("div");
    contentHolder.className = "nsl-content";

    if (typeof this.config.content === "string") {
      contentHolder.innerHTML = this.config.content;
    } else {
      contentHolder.appendChild(this.config.content);
    }
    this.modal.appendChild(contentHolder);

    // If close button option is true, add a close button
    if (this.config.closeButton === true) {
      this.closeButton = document.createElement("button");
      this.closeButton.className = "nsl-close close-button";
      this.closeButton.innerHTML = "cancel";
      this.modal.appendChild(this.closeButton);
    }

    // If overlay is true, add an overlay
    if (this.config.overlay === true) {
      this.overlay = document.createElement("div");
      this.overlay.className = "nsl-overlay " + this.config.className;
      docFrag.appendChild(this.overlay);
    }

    // append the modal to the document fragment
    docFrag.appendChild(this.modal);

    // Append the docfrag to the doc body
    document.body.appendChild(docFrag);
  }

  private initializeEvents(): void {
    if (this.closeButton)
      this.closeButton.addEventListener("click", this.close.bind(this));

    if (this.overlay)
      this.overlay.addEventListener("click", this.close.bind(this));
  }
}
