# NAU Social Login

Universal social login package for signing in users with Google and Facebook (more later).

This single package is all that is required. All dependencies and styles are included in the bundle.

## Demo

There is a handy demo included in the dist directory that shows a basic login cycle. If you have node installed, you can use the dev server by doing an `npm insttall` and then running `npm run server`. It will start a tiny dev server and the demo will be available at [http://localhost:3333/demo.html](http://localhost:3333/demo.html).

## How to Use

Include the package in your project, in a directory which your webserver can see. You can also simply use bower to get the newest version:

`bower install --save https://bitbucket.org/northernarizonauniversity/nau-social-login.git`

Then, include the main bundle script and css on your page, for example:

`<link rel="stylesheet" href="lib/nau-social-login/dist/main.css">` in the head

`<script src="lib/nau-social-login/dist/bundle.js"></script>` below the end of body

This will add an nau namespace to the browser global namespace containing a the `NAUSocialLogin` class.

### Initialization

After that, you can initialize the package and use it. Initialization is as follows:

    var nauSocial = new nau.NAUSocialLogin({
        app: 'my-application',
        key: 'myapikey'
    });

*(Contact the Enterprise Web Team or ITS Solution Center for help getting an API key and app name.)*

The package communicates with the outside world by firing events on the document element. You can use either jQuery or vanilla JavaScript events to catch these events.

Using jQuery: `$(document).on('onSignInSuccess.NAUSocialLogin', function (event, result) { ... }`

Or JavaScript: `document.addEventListener('onSignInSuccess.NAUSocialLogin', function (event) { ... }`

*(When using JavaScript events, the custom data sent to the event will be in the details key.)*

The following events are available:

* `onInitializeApp.NAUSocialLogin` - Fired when the package is ready to use.
* `onAuthStateChanged.NAUSocialLogin` - Fired when the user logs in or out.
* `onSignInSuccess.NAUSocialLogin` - Fired when the user successfully logs in, will be provided with a result and the user object.
* `onSignInFailure.NAUSocialLogin` - Fired when something goes wrong.
* `onProviderSelect.NAUSocialLogin` - Fired when the user chooses a login porvider.

You will mostly be interested in onSignInSuccess events, but the others are useful for knowing what's going on. On a successful login, the user information from the provider can be accessed at the `user` property on the object.

You also have the option of passing callback functions to the initialization options, like so:

    new nau.NAUSocialLogin({
        app: 'my-application',
        key: 'myapikey',
        onSignInSuccess: function (result) { ... },
        onSignInFailure: function (error) { ... }
    });

### API

Typically, once you have the package initialized and events set up, you will just want to call `NAUSocialLogin::signInWithModal()`. This method automatically opens the modal, allows the user to sign in, and then closes the modal. The following methods are also available:

#### NAUSocialLogin::signInWithModal()

Opens the provider menu modal, allows the user to sign in, then closes the modal.

#### NAUSocialLogin::signInWithPopup()

Direct access to the underlying Firebase object. Opens the given provider sign in popup directly.

#### NAUSocialLogin::openProviderMenuModal()

Opens the provider menu modal. You will have to handle events manually.

#### NAUSocialLogin::closeProviderMenuModal()

Closes the provider menu modal.

